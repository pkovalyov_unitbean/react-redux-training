import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux'
import {createStore, applyMiddleware} from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

import App from './App';
import './index.css';
import reducer from './reducers';
import About from './About';
/*const initialState={
tracks: [
'Smells like teen spirit',
'Mr. Sandman'
],
playlists:[
'My home playlist',
'My work playlist'
]
}*/

/*function playlist(state=initialState, action) {
//console.log(action);
  if(action.type==='ADD_TRACK'){
    return{
    ...state,
    tracks: [...state.tracks, action.payload]
    };
  }
  else if(action.type==='DELETE_TRACK')
    return state;
  else if(action.type==='ADD_PLAYLIST')
    return state;
  else if(action.type==='DELETE_PLAYLIST')
    return state;
  return state;}*/

  const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)));

ReactDOM.render(
<Provider store={store}>
      <Router >
             <div>
                 <div>
                     <Link to="/">Tracks</Link>
                     <Link to="/about">About</Link>
                 </div>
                 <Route exact path="/" component={App} />
                 <Route path="/about" component={About}/>
             </div>
         </Router>
</Provider>,
document.getElementById('root')
  );



// import {createStore} from 'redux';

// function playlist(state=[], action) {
// //console.log(action);
//   if(action.type==='ADD_TRACK'){
//     return[
//     ...state,
//     action.payload
//     ];
//   }
//   return state;}

// const store = createStore(playlist);
// const addTrackBtn=document.querySelectorAll('.addTrackBtn')[0];
// const trackInput=document.querySelectorAll('.trackInput')[0];
// const list=document.querySelectorAll('.list')[0];
// //console.log(store.getState());

// store.subscribe(()=>{
//console.log('subscribe', store.getState());
// list.innerHTML='';
// trackInput.value='';
// store.getState().forEach(track=>{
//   const li= document.createElement('li');
//   li.textContent = track;
//   list.appendChild(li);
// })
// })


// if(addTrackBtn) { //Без этой проверки выводится ошибка undefined
//   addTrackBtn.addEventListener('click',()=>{
// const trackName = trackInput.value;
// //console.log('trackName', trackName);
// store.dispatch({type: 'ADD_TRACK', payload: trackName});
// });
// }