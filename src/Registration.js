import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './RegistrationForm.css';


class RegistrationForm extends Component {
  constructor(props){
  super(props);
  this.state ={
    email: ''
    };
    this.handleEmailChange=this.handleEmailChange.bind(this);
    this.handleSubmit=this.handleSubmit.bind(this);
  }
  handleSubmit(event){
    event.preventDefault();
    console.log('Form submitted: ', this.state.email); 
  }
  handleEmailChange(event){
    console.log('E-mail was changed', this);
    this.setState({email: event.target.value});
  }
  render() {
    console.log('items', this.props.items);
    return (
    <form onSubmit={this.handleSubmit}>
    <input 
    type="text" 
    placeholder="E-mail" 
    value={this.state.email} 
    onChange={this.handleEmailChange}
    className="emailfield"
    />
    <button className="submitBtn">Save</button>
    </form>
    );
  }
}

export default RegistrationForm;